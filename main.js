// creatig the process.env file
const dotenv = require("dotenv");
dotenv.config();

const { Client, GatewayIntentBits, Collection } = require("discord.js");

const { DisTube } = require("distube");
const fs = require("node:fs");
const path = require("node:path");

// importing commands
const pingCommand = require("./commands/ping.js");

const client = new Client({
  intents: [
    GatewayIntentBits.Guilds,
    GatewayIntentBits.GuildMessages,
    GatewayIntentBits.MessageContent,
    GatewayIntentBits.GuildVoiceStates,
  ],
});
client.commands = new Collection();

client.DisTube = new DisTube(client, {
  // default recommended options (idk wtf is this)
  leaveOnStop: false,
  emitNewSongOnly: true,
  emitAddSongWhenCreatingQueue: false,
  emitAddListWhenCreatingQueue: false,
});

const prefix = "-";

// registering all the commands
client.commands = new Collection();
fs.readdir(path.join(__dirname, "/commands"), (err, files) => {
  if (err) {
    console.log(err);
  }

  // Retrieve only the files that end in .js
  const JsFiles = files.filter(
    (fileName) => fileName.split(".").pop() === "js"
  );

  for (const fileName of JsFiles) {
    const cmd = require(path.join(__dirname, `/commands/${fileName}`));
    client.commands.set(cmd.name, cmd);
  }
});

client.once("ready", () => {
  console.log("Cobzaru is online!");
});

client.on("messageCreate", (message) => {
  console.log(message.content.startsWith(prefix));
  console.log(message.author.id === client.user.id);
  if (
    !message.content.startsWith(prefix) ||
    message.author.id === client.user.id
  ) {
    return;
  }
  const args = message.content.slice(prefix.length).split(/ +/);
  const command = args.shift();

  console.log(args);
  console.log(command);

  const runCommand = client.commands.get(command);

  if (runCommand) {
    runCommand.command(message, args, client);
    return;
  }
  message.channel.send("Nu inteleg comanda bossule");
});

// Listeners
client.DisTube.on("playSong", (queue, song) => {
  const playEmbed = {
    color: 0x3f984c,
    title: `Now playing: ${song.name}`,
    description: `By ${song.uploader.name}`,
  };

  queue.textChannel.send({ embeds: [playEmbed] });
});

client.DisTube.on("addSong", (queue, song) => {
  const playEmbed = {
    color: 0x20c4c4,
    title: `Added to queue: ${song.name}`,
    description: `Position in queue - #${queue.songs.length - 1}`,
  };

  queue.textChannel.send({ embeds: [playEmbed] });
});

client.login(process.env.TOKEN);

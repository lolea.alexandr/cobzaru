module.exports = {
  name: "skip",
  command(message, _1, client) {
    const queue = client.DisTube.getQueue(message);

    if (queue.songs.length < 2) {
      queue.stop();
      return;
    }
    queue.skip();
  },
};
